#!/bin/bash

# Make directory hierarchy
mkdir script1;


mkdir script1/Pumpkin script1/Diakon script1/Carrot script1/Parsenip;

mkdir script1/Pumpkin/kabocha script1/Pumpkin/WinterMelon;
mkdir script1/Diakon/Pickle script1/Diakon/Oden;
mkdir script1/Carrot/Orange script1/Carrot/Oden;
mkdir script1/Parsenip/Chips script1/Parsenip/Soup;


# Setting permissions to directories
chmod a-xw script1/Pumpkin script1/Parsenip;
chmod go-rwx script1/Diakon;
chmod a-rwx script1/Carrot;

# Change permissions to directories so we can copy datasets
chmod a+xw script1/Pumpkin script1/Parsenip;
chmod go+rwx script1/Diakon;
chmod a+rwx script1/Carrot;

# copy data sets
cp ../Datasets/pg58.txt script1/Diakon/Pickle;
cp ../Datasets/pg972.txt script1/Carrot; 
cp ../Datasets/pg972.txt script1/Carrot/Oden;
cp ../Datasets/heart.csv script1/Pumpkin;
cp ../Datasets/heart.csv script1/Parsenip/Chips;

# Change the permissions back
chmod a-xw script1/Pumpkin script1/Parsenip;
chmod go-rwx script1/Diakon;
chmod a-rwx script1/Carrot;
