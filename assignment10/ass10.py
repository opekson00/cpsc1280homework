#!/usr/bin/python

import os
import subprocess
import sys

# Get current working path
home = os.getcwd()


# Accept a list of directories from stdin and chcek if any are not a directory
arguments = sys.argv
for argument in arguments[1:]:
    try:
        os.path.isfile(argument)
    except: 
        print("{} is not a directory".format(argument))


# For each directory find the total size of the files in the directory, not including files in the subdirectories, subdirectories, or symbolic links
# Produce listing to stdout

# Clear summary10.csv if has data

clear = open("{}/summary10.csv".format(home), "w")
clear.write("")

stdout = open("{}/summary10.csv".format(home), "a")
stdout.write("directory name,number of regular files,total size\n");

count = 0
sizeOfFiles = 0
for argument in arguments[1:]:
    os.chdir("{}/{}".format(home, argument))
    files = os.listdir()
    for file in files:
        if os.path.isfile(file):
            count += 1
            sizeOfFiles += os.path.getsize(file)
    stdout.write("{},{},{}\n".format(argument, count, sizeOfFiles))
    count = 0
    sizeOfFiles = 0
