#!/usr/bin/python

import os

# create a script that generates a set of test directories (at least 3). The directories should contain regular files, subdirectoreis, and symbolic links

# Create structure for test directory 1 (reguar file system)
os.mkdir("./dir1")
os.mkdir("./dir1/subd1")
os.mkdir("./dir1/subd1/subd2")

f = open("./dir1/f1", "w")
f.write("Hello, World!")
f2 = open("./dir1/f2", "w")
f2.write("sldjaflajsfdj;asfjasjfasjdfljsafjasfjd;asdjfdasjfldsajfljadslfjasklfj;asjf\n;sjlsfjddal;sjfdkasjflajsf\nslfjlasjflk;ajsfljasklfja;s")
open("./dir1/subd1/f1", "w")
open("./dir1/subd1/subd2/f1", "w")

os.symlink("./dir1/f2", "./dir1/subd1/f2")
os.symlink("./dir1/f2", "./dir1/subd1/subd2/f2")

# Create structure for test directory 2 (symbolic links in first depth)
os.mkdir("./dir2")
os.mkdir("./dir2/subd1")

f = open("./dir2/f1", "w")
f.write("Hello, World!\nHello\nHello\nWorld!\nasdfasfas")
f2 = open("./dir2/f2", "w")
f2.write(";;asdjfdasjfldsajfljadslfjasklfj;asjf\n;sjlsfjddal;sjfdkasjflajsf\nslfjlasjflk;ajsfljasklfja;s")
f3 = open("./dir2/subd1/f3", "w")
f3.write("This will be a symbolic link file")
f4 = open("./dir2/subd1/f4", "w")
f4.write("This will be another symbolic link file putting the expeceted file count to still be 2")

os.symlink("./dir2/subd1/f3", "./dir2/f3")
os.symlink("./dir2/subd1/f4", "./dir2/f4")

# Create structure for test directory 3 (only symbolic links no files)
os.mkdir("./dir3")
os.mkdir("./dir3/subd1")

f1 = open("./dir3/subd1/f1", "w")
f1.write("This will be a symbolic link file")
f2 = open("./dir3/subd1/f2", "w")
f2.write("This will be another symbolic link file putting the expeceted file count to still be 2")

os.symlink("./dir3/subd1/f1", "./dir3/f1")
os.symlink("./dir3/subd1/f2", "./dir3/f2")