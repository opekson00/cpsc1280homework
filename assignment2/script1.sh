#!/bin/bash

mkdir script1;

# Inode 100 (script1)

mkdir script1/Noah script1/Crab script1/Shrimp;

echo 'test file' > script1/rabbit;

# Inode 200 (Noah)
echo 'test file' > script1/Noah/cheese;
echo 'test file' > script1/Noah/brimble;
echo 'test file' > script1/Noah/pepper;
echo 'test file' > script1/Noah/rice;
echo 'test file' > script1/Noah/beans;


# Inode 270 (Crab)
mkdir script1/Crab/Conclave script1/Crab/Senate;

echo 'test file' > script1/Crab/Coven;
echo 'test file' > script1/Crab/Diet;
echo 'test file' > script1/Crab/Convention;


# Inode 300 (Crab/Senate)
echo 'test file' > script1/Crab/Senate/TrainerTips;
echo 'test file' > script1/Crab/Senate/EngineeringExplained;

ln script1/Crab/Coven script1/Crab/Senate/CathyCat;

# Inode 350 (Crab/Conclave)
echo 'test file' > script1/Crab/Conclave/Chocolate;
echo 'test file' > script1/Crab/Conclave/Caramel;
ln script1/Crab/Convention script1/Crab/Conclave/Strawberry;

# Inode 700 (Shrimp)
echo 'test file' > script1/Shrimp/steel;
echo 'test file' > script1/Shrimp/copper;

mkdir script1/Shrimp/iron;

# Inode 240 (Shrimp/iron)
echo 'test file' > script1/Shrimp/iron/tiger;
echo 'test file' > script1/Shrimp/iron/rabbit;
echo 'test file' > script1/Shrimp/iron/wolf;
echo 'test file' > script1/Shrimp/iron/rat;


# Permissions (Starting from leaf files and directories in the file system)
chmod u-w script1/Shrimp/iron/tiger script1/Shrimp/iron/rabbit;

chmod u+x script1/Shrimp/copper;
chmod u-w script1/Shrimp/copper script1/Shrimp/iron;

chmod u+x script1/Crab/Conclave/Chocolate;
chmod u-w script1/Crab/Conclave/Caramel;

chmod u+x script1/Crab/Senate/CathyCat;
chmod u-w script1/Crab/Senate/TrainerTips script1/Crab/Senate/EngineeringExplained;

chmod u+x script1/Crab/Coven;
chmod u-w script1/Crab/Senate;

chmod u+x script1/Noah/cheese script1/Noah/brimble script1/Noah/pepper script1/Noah/rice script1/Noah/beans;

chmod u+x script1/rabbit;
chmod u-wx script1/Crab;
chmod u-w script1/Shrimp;

