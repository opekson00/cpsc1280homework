#!/bin/bash

# delete test data if it exists
# rm -r testDir;


# delete all files named $2
find $1 -name $2 -type f -exec rm -v > $3 {} \;

# delete all subdirectories in $1 not named $2
find $1/* ! -name $2 -type d -exec rm -rv >> $3 {} \;


