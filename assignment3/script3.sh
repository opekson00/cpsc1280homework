#!/bin/bash

# create $2 if it doesn't exist and if it does remove it then create it
find . -name $2 -type d -exec rm -r {} \;

mkdir $2;

#replicate the directory structure of $1 inside $2
cp -R $1 $2

#find all files in $1 and hard link them to $2
find $1/* -type f -exec echo >> f1.txt {} \;
find "${2}/${1}" -type f -exec echo >> f2.txt {} \;

paste f1.txt f2.txt | xargs ln ;

rm f1.txt f2.txt;