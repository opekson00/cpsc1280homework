#!/bin/bash

#find 13 files with the most hard links

find $1 -maxdepth 1 -type f -ls  | sort -nr -k 4 | head -n13 | tr -s ' ' | cut -d" " -f1,4,11

# find all file references for file at the top
file=$(find $1 -maxdepth 1 -type f -ls  | sort -nr -k 4 | head -n1 | tr -s ' ' | cut -d" " -f11) 
inodeNumber=$(find $1 -maxdepth 1 -type f -ls  | sort -nr -k 4 | head -n1 | tr -s ' ' | cut -d" " -f1)

find $1 -inum $inodeNumber;