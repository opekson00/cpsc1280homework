#!/bin/bash

# get gender
cat births.csv | cut -d'"' -f12 > gender.txt;

# get year
cat births.csv | cut -d'"' -f2 > year.txt

# get Neighborhood

cat births.csv | cut -d'"' -f8 > neighborhood.txt;

# get Neighborhood Name
cat births.csv | cut -d'"' -f10 > neighborhoodName.txt;

# get Numbers
cat births.csv | cut -d'"' -f13-14 | tr -d "," | tr -d '\"' > numbers.txt;

# get total number of births
totalNumberOfBirths=$(tail -n +2 numbers.txt | paste -sd+ | bc);

# get district codes
cat births.csv | cut -d'"' -f4 > districtCode.txt;

# put number of births in first line of file
echo NumberOfBirths = $totalNumberOfBirths > $1

# sort rows by district code 
paste -d"," gender.txt year.txt neighborhood.txt neighborhoodName.txt numbers.txt districtCode.txt | sort -t, -Vr -k6 | cut -d"," -f1-5 >> $1

rm gender.txt year.txt neighborhood.txt neighborhoodName.txt numbers.txt districtCode.txt

