#!/bin/bash

#compare the 2 directories


# listing of files only in the fist directory
comm -32 <(ls $1) <(ls $2) > $3;

# listing of files only in the second directory
comm -31 <(ls $1) <(ls $2) > $4;

# listing of files in both directories
comm -12 <(ls $1) <(ls $2) > $5;