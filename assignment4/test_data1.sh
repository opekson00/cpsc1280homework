#!/bin/bash

#test data for normal conditions

#creates a top level directory

mkdir tdata1;
touch tdata1/f1 tdata1/f2 tdata1/f3 tdata1/f4 tdata1/f5 tdata1/f6 tdata1/f7 tdata1/f8 tdata1/f9  tdata1/f10  tdata1/f11 tdata1/f12 tdata1/f13 tdata1/f14 tdata1/f15 tdata1/f16

# create many levels of subdirectories
mkdir tdata1/dir1 tdata1/dir2 tdata1/dir3 tdata1/dir4 tdata1/dir5 tdata1/dir6 tdata1/dir7 tdata1/dir8;

touch tdata1/dir1/f1  tdata1/dir1/f3 tdata1/dir1/f5 tdata1/dir1/f8 tdata1/dir1/f9;

touch tdata1/dir2/f9 tdata1/dir2/f1 tdata1/dir2/f4 tdata1/dir2/f7;

touch tdata1/dir7/f1 tdata1/dir7/f3 tdata1/dir7/f5 tdata1/dir7/f7;

# create files with different number of hardlinks
ln tdata1/f1 tdata1/dir4/f1.h;
ln tdata1/f1 tdata1/dir3/f1.h;
ln tdata1/f1 tdata1/dir2/f1.h;
ln tdata1/f1 tdata1/dir8/f1.h;

ln tdata1/f2 tdata1/dir4/f2.h;
ln tdata1/f2 tdata1/dir3/f2.h;
ln tdata1/f2 tdata1/dir2/f2.h;
ln tdata1/f2 tdata1/dir8/f2.h;

ln tdata1/f3 tdata1/dir4/f3.h;
ln tdata1/f3 tdata1/dir3/f3.h;
ln tdata1/f3 tdata1/dir2/f3.h;
ln tdata1/f3 tdata1/dir8/f3.h;

ln tdata1/f4 tdata1/dir3/f4.h;
ln tdata1/f4 tdata1/dir2/f4.h;
ln tdata1/f4 tdata1/dir8/f4.h;
ln tdata1/f4 tdata1/dir4/f4.h;

ln tdata1/f5 tdata1/dir4/f5.h;
ln tdata1/f5 tdata1/dir3/f5.h;
ln tdata1/f5 tdata1/dir2/f5.h;
ln tdata1/f5 tdata1/dir8/f5.h;


ln tdata1/f6 tdata1/dir2/f6.h;
ln tdata1/f6 tdata1/dir4/f6.h;

ln tdata1/f7 tdata1/dir1/f7.h;
ln tdata1/f7 tdata1/dir2/f7.h;
ln tdata1/f7 tdata1/dir3/f7.h;
ln tdata1/f7 tdata1/dir4/f7.h;
ln tdata1/f7 tdata1/dir5/f7.h;
ln tdata1/f7 tdata1/dir6/f7.h;
ln tdata1/f7 tdata1/dir7/f7.h;

ln tdata1/f8 tdata1/dir1/f8.h;
ln tdata1/f8 tdata1/dir4/f8.h;
ln tdata1/f8 tdata1/dir6/f8.h;
ln tdata1/f8 tdata1/dir7/f8.h;

ln tdata1/f9 tdata1/dir2/f9.h;
ln tdata1/f9 tdata1/dir3/f9.h;
ln tdata1/f9 tdata1/dir4/f9.h;
ln tdata1/f9 tdata1/dir5/f9.h;
ln tdata1/f9 tdata1/dir6/f9.h;

ln tdata1/f10 tdata1/dir1/f10.h;
ln tdata1/f10 tdata1/dir2/f10.h;
ln tdata1/f10 tdata1/dir3/f10.h;
ln tdata1/f10 tdata1/dir8/f10.h;
ln tdata1/f10 tdata1/dir5/f10.h;
ln tdata1/f10 tdata1/dir6/f10.h;
ln tdata1/f10 tdata1/dir4/f10.h;

ln tdata1/f11 tdata1/dir1/f11.h;
ln tdata1/f11 tdata1/dir2/f11.h;
ln tdata1/f11 tdata1/dir3/f11.h;
ln tdata1/f11 tdata1/dir8/f11.h;
ln tdata1/f11 tdata1/dir5/f11.h;
ln tdata1/f11 tdata1/dir6/f11.h;
ln tdata1/f11 tdata1/dir4/f11.h;

ln tdata1/f12 tdata1/dir1/f12.h;
ln tdata1/f12 tdata1/dir2/f12h;
ln tdata1/f12 tdata1/dir3/f12.h;
ln tdata1/f12 tdata1/dir4/f12.h;
ln tdata1/f12 tdata1/dir5/f12.h;
ln tdata1/f12 tdata1/dir6/f12.h;
ln tdata1/f12 tdata1/dir7/f12.h;
ln tdata1/f12 tdata1/dir8/f12.h;
ln tdata1/f12 tdata1/dir2/f12.hh;
ln tdata1/f12 tdata1/dir3/f12.hh;
ln tdata1/f12 tdata1/dir8/f12.hh;
ln tdata1/f12 tdata1/dir5/f12.hh;
ln tdata1/f12 tdata1/dir6/f12.hh;
ln tdata1/f12 tdata1/dir4/f12.hh;

ln tdata1/f13 tdata1/dir1/f13.h;
ln tdata1/f13 tdata1/dir2/f13.h;

ln tdata1/f14 tdata1/dir3/f14.h;
ln tdata1/f14 tdata1/dir2/f14.hh;

ln tdata1/f15 tdata1/dir6/f15.h;
ln tdata1/f15 tdata1/dir7/f15.h;
ln tdata1/f15 tdata1/dir8/f15.h;

ln tdata1/f16 tdata1/dir1/f16.h;
ln tdata1/f16 tdata1/dir5/f16.h;
ln tdata1/f16 tdata1/dir6/f16.h;
ln tdata1/f16 tdata1/dir4/f16.hh;