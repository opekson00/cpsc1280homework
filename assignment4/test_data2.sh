#!/bin/bash

#test data for entries with less than 13 files

#creates a top level directory

mkdir tdata2;
touch tdata2/f1 tdata2/f2 tdata2/f3 tdata2/f4 tdata2/f5 tdata2/f6 

# create many levels of subdirectories
mkdir tdata2/dir1 tdata2/dir2 

touch tdata2/dir1/f1  tdata2/dir1/f3 tdata2/dir1/f5 tdata2/dir1/f8 tdata2/dir1/f9;
touch tdata2/dir2/f9 tdata2/dir2/f1 tdata2/dir2/f4 tdata2/dir2/f7;


# create files with different number of hardlinks
ln tdata2/f1 tdata2/dir1/f1.h;
ln tdata2/f1 tdata2/dir2/f1.hh;
ln tdata2/f1 tdata2/dir2/f1.hhh;
ln tdata2/f1 tdata2/dir1/f1.hhhh;

ln tdata2/f2 tdata2/dir1/f2.h;
ln tdata2/f2 tdata2/dir2/f2.hhh;

ln tdata2/f3 tdata2/dir1/f3.h;
ln tdata2/f3 tdata2/dir2/f3.hh;
ln tdata2/f3 tdata2/dir2/f3.hhh;
ln tdata2/f3 tdata2/dir1/f3.hhhh;

ln tdata2/f4 tdata2/dir1/f4.h;

ln tdata2/f5 tdata2/dir1/f5.h;
ln tdata2/f5 tdata2/dir2/f5.hh;
ln tdata2/f5 tdata2/dir2/f5.hhh;
ln tdata2/f5 tdata2/dir1/f5.hhhh;
ln tdata2/f5 tdata2/dir1/f5.aa;
ln tdata2/f5 tdata2/dir2/f5.ha;
ln tdata2/f5 tdata2/dir2/f5.ah;
ln tdata2/f5 tdata2/dir1/f5.aaahhh;
ln tdata2/f5 tdata2/dir1/f5.sh;
ln tdata2/f5 tdata2/dir2/f5.dhh;
ln tdata2/f5 tdata2/dir2/f5.hfhh;
ln tdata2/f5 tdata2/dir1/f5.hshhh;

ln tdata2/f6 tdata2/dir1/f6.sh;
ln tdata2/f6 tdata2/dir2/f6.hdh;
ln tdata2/f6 tdata2/dir2/f6.hhh;
ln tdata2/f6 tdata2/dir1/f6.hhhhhdhh;
ln tdata2/f6 tdata2/dir1/f6.h;
ln tdata2/f6 tdata2/dir2/f6.hah;
ln tdata2/f6 tdata2/dir2/f6.hshh;
ln tdata2/f6 tdata2/dir1/f6.hhdhh;
