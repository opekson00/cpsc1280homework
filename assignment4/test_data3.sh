#!/bin/bash

#more normal test data

#creates a top level directory

mkdir tdata3;
touch tdata3/f1 tdata3/f2 tdata3/f3 tdata3/f4 tdata3/f5 tdata3/f6 tdata3/f7 tdata3/f8 tdata3/f9  tdata3/f10  tdata3/f11 tdata3/f12 tdata3/f13 tdata3/f14 tdata3/f15 tdata3/f16

# create many levels of subdirectories
mkdir tdata3/dir1 tdata3/dir2 tdata3/dir3 tdata3/dir4 tdata3/dir5 tdata3/dir6 tdata3/dir7 tdata3/dir8;

touch tdata3/dir1/f1  tdata3/dir1/f3 tdata3/dir1/f5 tdata3/dir1/f8 tdata3/dir1/f9;

touch tdata3/dir2/f9 tdata3/dir2/f1 tdata3/dir2/f4 tdata3/dir2/f7;

touch tdata3/dir7/f1 tdata3/dir7/f3 tdata3/dir7/f5 tdata3/dir7/f7;

# create files with different number of hardlinks
ln tdata3/f1 tdata3/dir1/f1.sh;
ln tdata3/f1 tdata3/dir2/f1.ah;
ln tdata3/f1 tdata3/dir3/f1.h;
ln tdata3/f1 tdata3/dir4/f1.sh;

ln tdata3/f2 tdata3/dir1/f2.h;
ln tdata3/f2 tdata3/dir2/f2.fh;
ln tdata3/f2 tdata3/dir3/f2.h;
ln tdata3/f2 tdata3/dir4/f2.h;

ln tdata3/f3 tdata3/dir1/f3.hgg;
ln tdata3/f3 tdata3/dir2/f3.h;
ln tdata3/f3 tdata3/dir3/f3.h;
ln tdata3/f3 tdata3/dir4/f3.hhh;

ln tdata3/f4 tdata3/dir1/f4.ggh;
ln tdata3/f4 tdata3/dir2/f4.hhhh;
ln tdata3/f4 tdata3/dir3/f4.jjh;
ln tdata3/f4 tdata3/dir4/f4.jh;

ln tdata3/f5 tdata3/dir1/f5.sssh;
ln tdata3/f5 tdata3/dir2/f5.ssh;
ln tdata3/f5 tdata3/dir3/f5.hg;
ln tdata3/f5 tdata3/dir4/f5.h;


ln tdata3/f6 tdata3/dir1/f6.h;
ln tdata3/f6 tdata3/dir2/f6.h;

ln tdata3/f7 tdata3/dir1/f7.gh;
ln tdata3/f7 tdata3/dir2/f7.gh;
ln tdata3/f7 tdata3/dir3/f7.fh;
ln tdata3/f7 tdata3/dir4/f7.sgh;
ln tdata3/f7 tdata3/dir5/f7.h;
ln tdata3/f7 tdata3/dir6/f7.sfh;
ln tdata3/f7 tdata3/dir7/f7.h;

ln tdata3/f8 tdata3/dir1/f8.h;
ln tdata3/f8 tdata3/dir2/f8.hh;
ln tdata3/f8 tdata3/dir3/f8.fh;
ln tdata3/f8 tdata3/dir4/f8.h;

ln tdata3/f9 tdata3/dir1/f9.h;
ln tdata3/f9 tdata3/dir2/f9.dh;
ln tdata3/f9 tdata3/dir3/f9.h;
ln tdata3/f9 tdata3/dir4/f9.h;
ln tdata3/f9 tdata3/dir5/f9.h;

ln tdata3/f10 tdata3/dir1/f10.h;
ln tdata3/f10 tdata3/dir2/f10.hhh;
ln tdata3/f10 tdata3/dir3/f10.werh;
ln tdata3/f10 tdata3/dir4/f10.h;
ln tdata3/f10 tdata3/dir5/f10.whw;
ln tdata3/f10 tdata3/dir6/f10.shh;
ln tdata3/f10 tdata3/dir7/f10.h;

ln tdata3/f11 tdata3/dir1/f11.h;
ln tdata3/f11 tdata3/dir2/f11.h;
ln tdata3/f11 tdata3/dir3/f11.hh;
ln tdata3/f11 tdata3/dir4/f11.hw;
ln tdata3/f11 tdata3/dir5/f11.wfh;
ln tdata3/f11 tdata3/dir6/f11.h;
ln tdata3/f11 tdata3/dir7/f11.asah;

ln tdata3/f12 tdata3/dir1/f12.h;
ln tdata3/f12 tdata3/dir2/f12h;
ln tdata3/f12 tdata3/dir3/f12.dh;
ln tdata3/f12 tdata3/dir4/f12.h;
ln tdata3/f12 tdata3/dir5/f12.ewwsh;
ln tdata3/f12 tdata3/dir6/f12.h;
ln tdata3/f12 tdata3/dir7/f12.hh;
ln tdata3/f12 tdata3/dir8/f12.h;
ln tdata3/f12 tdata3/dir2/f12.hbh;
ln tdata3/f12 tdata3/dir3/f12.hh;
ln tdata3/f12 tdata3/dir8/f12.hh;
ln tdata3/f12 tdata3/dir5/f12.hshh;
ln tdata3/f12 tdata3/dir6/f12.hh;
ln tdata3/f12 tdata3/dir4/f12.hh;
ln tdata3/f12 tdata3/dir1/f12.yh;
ln tdata3/f12 tdata3/dir2/f1.yh;
ln tdata3/f12 tdata3/dir3/f12.ydh;
ln tdata3/f12 tdata3/dir4/f12.hy;
ln tdata3/f12 tdata3/dir5/f12.ewywsh;
ln tdata3/f12 tdata3/dir6/f12.hy;
ln tdata3/f12 tdata3/dir7/f12.hyh;
ln tdata3/f12 tdata3/dir8/f12.yh;
ln tdata3/f12 tdata3/dir2/f12.hybh;
ln tdata3/f12 tdata3/dir3/f12.hyh;
ln tdata3/f12 tdata3/dir8/f12.hyh;
ln tdata3/f12 tdata3/dir5/f12.hysshh;
ln tdata3/f12 tdata3/dir6/f12.hyh;
ln tdata3/f12 tdata3/dir4/f12.hyh;
ln tdata3/f13 tdata3/dir1/f13.hy;
ln tdata3/f13 tdata3/dir2/f13.hy;

ln tdata3/f14 tdata3/dir1/f14.h;
ln tdata3/f14 tdata3/dir2/f14.hh;


ln tdata3/f15 tdata3/dir1/f15.fah;
ln tdata3/f15 tdata3/dir2/f15.wh;
ln tdata3/f15 tdata3/dir3/f15.sh;


ln tdata3/f16 tdata3/dir1/f16.gh;
ln tdata3/f16 tdata3/dir2/f16.weh;
ln tdata3/f16 tdata3/dir3/f16.asfh;
ln tdata3/f16 tdata3/dir4/f16.hh;
