#!/bin/bash

#find all items whose PRODUCT_LITRES_PER_CONTAINER is greater than 1L.

# From using cut we know it is in field 9 when delimited by comma

cat BC_Liquor_Store_Product_Price_List.csv | cut -d"," -f7,9 | sort -n -t ',' -k 6 | grep -v "0." | grep -v "1$" | grep -E "[.1-9]*"
