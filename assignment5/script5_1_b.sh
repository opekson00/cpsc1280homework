#!/bin/bash

# Write a script called, script5_1_b.sh, that takes in two parameters. The first parameter is the PRODUCT_COUNTRY_ORIGIN_NAME and the second parameter is PRODUCT_CLASS_NAME. The script will find all products that is from the specified country and of the specified type

# Produc_country_origin_name is field 5 with delimeter ','
# Product_class_name is field 2 with delimeter ','
# Product_Long_Name is field 7 with delimeter ','


cat BC_Liquor_Store_Product_Price_List.csv | cut -d"," -f2,5,7 | grep "$1,$2" ;