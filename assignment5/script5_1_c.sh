#!/bin/bash

# Write a script called, script5_1_c.sh, that takes in 3 parameters. Each of the 3 parameters is a PRODUCT_COUNTRY_ORIGIN_NAME. Find all products from any of the 3 countries

# Produc_country_origin_name is field 5 with delimeter ','
# Product_Long_Name is field 7 with delimeter ','

cat BC_Liquor_Store_Product_Price_List.csv | cut -d"," -f5,7 | grep -E "^($1|$2|$3)" ;