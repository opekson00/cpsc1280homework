#!/bin/bash

# Write a script called, script5_1_d.sh, that finds all products that are between $20 and $200
# Product_Long_Name is field 7 with delimeter ','
# Product_display price is field 12 with delimeter ','


# 1. grep -v "[0-9][.].*$" - remove all numbers that are single digits with decimal points
# 2. grep -v ",[0-9]$" - removes all single digit numbers
# 3. grep -v ",[1][0-9]$" - removes all double digit numbers from 10-19
# 4. grep -v ",[1][0-9][.].*$" - removes all double digit number from 10-19 that have decimal points
# 5. grep -v ",[1-9][0-9][0-9][0-9]" - removes all numbers more than 3 digits
# 6. grep -v ",[3-9][0-9][0-9]" - removes all digits higher than 299-999
# 7. grep -v ",[2][0-9][1-9]" and  grep -v ",[2][1-9][0-9]" - removes all digits higher than 201
# 8. grep -Ev ",[2][0][0].+" - removes all digits that are 200 but have decimal
# 9. grep -v ",$" - remove all that have no price

cat BC_Liquor_Store_Product_Price_List.csv | cut -d"," -f7,12  | sort -n -t ',' -k 2 | grep -v ",[0-9][.].*$" | grep -v ",[0-9]$" | grep -v ",[1][0-9]$" | grep -v ",[1][0-9][.].*$" |  grep -v ",[1-9][0-9][0-9][0-9]" | grep -v ",[3-9][0-9][0-9]"  | grep -v ",[2][0-9][1-9]"  | grep -v ",[2][1-9][0-9]"  | grep -Ev ",[2][0][0].+" | grep -v ",$"; 


