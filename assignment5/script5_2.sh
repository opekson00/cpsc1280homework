#!/bin/bash

#Write a script called script5_2.sh that takes in 3 parameters. The first is a directory and the second is a search string we will call str1 and the third parameter is a search string we will call str2.  
# The script will find all the subdirectories that contain files with str1 and str2, that is both str1 and str2 must be in the same file.   That is the script will create a list of subdirectories

# -r is a recursive search
grep -rl "$2" ./$1 | xargs grep -rl "$3";