#!/bin/bash

# Write a script called script5_3.sh , that takes in 2 parameters. The 1st parameter is a filename, second parameter is an accident id. 
# Assuming that the files the script deals with files similar format to the accidents_2017.csv found in the Datasets/BarcelonaDataSet.  Given the Accident ID, find all accidents that occurred on the same day

# accident id is field 1 on delimeter ','
# day is field 7 on delimeter ','


day=$(cat ./$1 | grep $2 | cut -d"," -f5-7;)

grep "$day" ./$1;