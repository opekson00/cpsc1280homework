#!/bin/bash

filename=$1;
nameOfBird=$2;

sed -n "/$nameOfBird/,/_$/p" $filename | sed '$d';