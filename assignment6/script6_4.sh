#!/bin/bash

# Find the number of peole from "Ciutat Vella" (total)
total=$(grep '^"Ciutat Vella"' immigrants_emigrants_by_destination2.csv | wc -l) ;
header=1;
lineToInsert=$(($total + $header));

#Append the total  number of people from "Ciutat Vella" after the "Ciutat Vella" section.
sed "$lineToInsert a $total" immigrants_emigrants_by_destination2.csv;

