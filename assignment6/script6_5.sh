#!/bin/bash

filename=$1;


#find names of files to be replaced 
grep -o "[a-zA-Z]*.h" $1 > header.txt;

# find the include statement and replace it with header file name

# hard codeded 1 header file not sure how to do multiple
sed -e "/#include <header.h>/ r header.h" -e "s/#include <header.h>//" $1

rm header.txt

