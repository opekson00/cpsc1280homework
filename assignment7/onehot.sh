#!/bin/bash


filename=$1;

# First get the format of the table by knowing how many unique categories there are and their names
categoryCount=$(tail -n+2 $filename  | cut -d"," -f2 | sort | uniq | wc -l;);
categoryNames=$(tail -n+2 $filename | cut -d"," -f2 | sort | uniq;);

# output the new format header
cut -d"," -f1 $filename | head -n 1 > header.txt;
echo $categoryNames >> header.txt;
tr '\n' ',' < header.txt | tr ',' ' ' | tr ' ' ',' | sed 's/.$//' > onehot.csv
#add new line to end of the file
echo "" >> onehot.csv;

# get all values column
tail -n+2 $filename | sort | cut -d"," -f1 > values.txt;

# loop through each line and if first category in array equals the category then put 1 else 0 for that column

categoryArray=( $categoryNames )


categoryRange=$(expr $categoryCount - 1);

# make column names to insert data
for number in $(seq 0 $categoryRange)  ; do
    
    > category$number.txt
    
    name="${categoryArray[$number]}";
    echo $name
    
   tail -n+2 $filename | sort | cut -d"," -f2 | 
    while read line; do
        

        if [ $line == $name ]; then
            echo "1" >> category$number.txt
        else 
            echo "0" >> category$number.txt
        fi
    done 
    
done

# merge all files created 
fileList=$(find . -name "category*" | sort);


paste -d"," values.txt $fileList > rows.txt;

cat rows.txt >> onehot.csv;

# remove all unecessary files
rm header.txt;
rm category*;
rm rows.txt;
rm values.txt;