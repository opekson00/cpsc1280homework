#!/bin/bash

# adds all changes made to repo
git add .;
# creates a commit with the commit message
git commit -m "$*";
# push it to the upstream repo
git push;

# update